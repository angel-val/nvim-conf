return {
    "nvim-tree/nvim-tree.lua",
    lazy = false,
    priority = 60,
    config = function()
        require("nvim-tree").setup({
            hijack_cursor = true,
            sync_root_with_cwd = true,
            reload_on_bufenter = true,
            select_prompts = true,
            view = {
                preserve_window_proportions = true,
                width = 35,
            },
            renderer = {
                group_empty = true,
                full_name = true,
                root_folder_label = ":~:s?$?/?",
                highlight_git = "name",
                highlight_hidden = "name",
                indent_markers = { enable = true },
                icons = {
                    show = { hidden = true },
                    glyphs = {
                        bookmark = Icons.bookmark,
                        hidden = Icons.hidden,
                        git = {
                            unstaged = Icons.git.unstaged,
                            staged = Icons.git.staged,
                        },
                    },
                },
            },
            update_focused_file = {
                enable = true,
            },
            git = {
                show_on_open_dirs = false,
            },
            diagnostics = {
                enable = true,
                show_on_dirs = true,
                show_on_open_dirs = false,
                icons = Icons.diagnostics,
            },
            actions = {
                use_system_clipboard = false,
                change_dir = { global = true },
                remove_file = { close_window = false },
            },
            tab = {
                sync = {
                    open = true,
                    close = true,
                },
            },
        })

        local group = Util.augroup("NvimTreeUtils")

        -- Automatic closing
        Util.autocmd(group, "WinClosed", "Quit if nvimtree is the last window", function()
            local winnr = tonumber(vim.fn.expand("<amatch>")) or 0
            local api = require("nvim-tree.api")
            local tabnr = vim.api.nvim_win_get_tabpage(winnr)
            local bufnr = vim.api.nvim_win_get_buf(winnr)
            local buf_info = vim.fn.getbufinfo(bufnr)[1]
            local tab_wins = vim.tbl_filter(function(w)
                return w ~= winnr
            end, vim.api.nvim_tabpage_list_wins(tabnr))
            local tab_bufs = vim.tbl_map(vim.api.nvim_win_get_buf, tab_wins)
            if buf_info.name:match(".*NvimTree_%d*$") then -- close buffer was nvim tree
                -- Close all nvim tree on :q
                if not vim.tbl_isempty(tab_bufs) then -- and was not the last window (not closed automatically by code below)
                    api.tree.close()
                end
            else -- else closed buffer was normal buffer
                if #tab_bufs == 1 then -- if there is only 1 buffer left in the tab
                    local last_buf_info = vim.fn.getbufinfo(tab_bufs[1])[1]
                    if last_buf_info.name:match(".*NvimTree_%d*$") then -- and that buffer is nvim tree
                        vim.schedule(function()
                            if #vim.api.nvim_list_wins() == 1 then -- if its the last buffer in vim
                                vim.cmd("quit") -- then close all of vim
                            else -- else there are more tabs open
                                vim.api.nvim_win_close(tab_wins[1], true) -- then close only the tab
                            end
                        end)
                    end
                end
            end
        end, { nested = true })
    end,
    keys = {
        {
            "<leader>n",
            function()
                require("nvim-tree.api").tree.open()
            end,
        },
    },
}
