local format_group = Util.augroup("LSPFormat")

Util.autocmd(Util.augroup("LSPOnAttach"), "LspAttach", "LSP on_attach hook", function(event)
    local bufnr = event.buf
    local client = vim.lsp.get_client_by_id(event.data.client_id)

    if client ~= nil then
        if client.supports_method("textDocument/formatting", { bufnr = bufnr }) then
            Util.autocmd(format_group, "BufWritePre", "LSP formatting on save", function()
                vim.lsp.buf.format({ async = false })
            end, { buffer = bufnr, nested = true })
        end
    end

    local function trouble(mode)
        return function()
            require("trouble").open({ mode = mode })
        end
    end

    -- See `:help vim.lsp.*` for documentation on functions
    Util.maps({
        { "<C-a>", vim.lsp.buf.code_action },
        { "<C-a>", vim.lsp.buf.code_action, "v" },
        { "<C-r>", vim.lsp.buf.rename },
        { "K", vim.lsp.buf.hover },
        { "gD", trouble("lsp_declarations") },
        { "gd", trouble("lsp_definitions") },
        { "gi", trouble("lsp_implementations") },
        { "gr", trouble("lsp_references") },
        { "gT", trouble("lsp_type_definitions") },
    }, "n", { silent = true, buffer = bufnr })
end)
