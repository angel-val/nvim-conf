vim.g.mapleader = " "

local o = vim.opt
local g = vim.g

-- Navigation

o.scrolloff = 5
o.mouse = "a"
o.autoread = true -- Automatically update on file changes

-- Windowing

o.splitbelow = true
o.splitright = true
o.splitkeep = "topline"
o.pumheight = 10
o.modeline = false

-- UI

o.colorcolumn = "80"
o.conceallevel = 0 -- No hidden characters
o.showtabline = 2
o.laststatus = 3
o.showmode = false -- It's on the lualine
o.number = true
o.relativenumber = true
o.termguicolors = true

-- Whitespace

o.wrap = false
o.whichwrap:append("<,>,[,]") -- Arrow keys wrap lines
o.iskeyword:append({ "-" })

o.expandtab = true
o.smartindent = true
o.shiftwidth = 4
o.tabstop = 4
o.softtabstop = 4

-- Search

o.ignorecase = true
o.smartcase = true

o.incsearch = true
o.hlsearch = false

-- Folding

o.foldmethod = "expr"
o.foldexpr = "v:lua.vim.treesitter.foldexpr()"
o.foldlevel = 999

-- G

g.c_syntax_for_h = 1
g.undotree_windowlayout = 3
g.undotree_splitwidth = 50

-- LSP diagnostics

vim.diagnostic.Opts = {
    update_in_insert = true,
}
