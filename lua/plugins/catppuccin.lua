return {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
    opts = {
        flavour = "mocha",
        styles = {
            comments = {},
            conditionals = {},
            loops = {},
            functions = {},
            keywords = {},
            strings = {},
            variables = {},
            numbers = {},
            booleans = {},
            properties = {},
            types = {},
            operators = {},
        },
        integrations = {
            alpha = true,
            barbecue = {
                dim_dirname = true,
                bold_basename = false,
                dim_context = true,
                alt_background = false,
            },
            blink_cmp = true,
            gitsigns = true,
            indent_blankline = {
                enabled = true,
                colored_indent_levels = true,
            },
            markdown = true,
            mini = { enabled = true },
            native_lsp = {
                enabled = true,
                inlay_hints = {
                    background = false,
                },
            },
            nvimtree = true,
            treesitter = true,
            telescope = {
                enabled = true,
                style = "nvchad",
            },
            lsp_trouble = true,
        },
    },
    init = function()
        vim.cmd.colorscheme("catppuccin")
    end,
}
