local v = vim
local o = v.o
local wo = v.wo
local cmd = v.cmd

-- Autocmds and utilities for working with terminals

local group = Util.augroup("TermanAutocmds")

local oldsettings = {
    number = o.number,
    relativenumber = o.relativenumber,
    scrolloff = o.scrolloff,
    winbar = o.winbar,
}

Util.autocmd(
    group,
    {
        -- "BufWinEnter",
        -- "WinEnter",
        -- "TermOpen",
        "BufEnter",
    },
    "Declutter and force insert mode for terminals",
    function()
        v.schedule(function()
            if v.bo.buftype == "terminal" then
                wo.number = false
                wo.relativenumber = false
                wo.scrolloff = 0
                wo.winbar = ""
                cmd.startinsert()
            end
        end)
    end
)

Util.autocmd(group, "BufWinLeave", "Reclutter on changing buffer type", function()
    if v.bo.buftype == "terminal" then
        wo.number = oldsettings.number
        wo.relativenumber = oldsettings.relativenumber
        wo.scrolloff = oldsettings.scrolloff
        wo.winbar = oldsettings.winbar
    end
end)
