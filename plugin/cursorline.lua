local group = Util.augroup("CursorlineManager")

vim.opt.cursorline = true -- On by default

Util.autocmd(group, "WinEnter", "Automatically enable cursorline", function()
    vim.wo.cursorline = true
end)

Util.autocmd(group, "WinLeave", "Automatically disable cursorline", function()
    vim.wo.cursorline = false
end)
