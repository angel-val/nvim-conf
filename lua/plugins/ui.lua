return {
    {
        "stevearc/dressing.nvim",
        opts = {
            input = {
                border = { " ", " ", "", "", "", "", "", " " },
                width = 10,
                mappings = {
                    i = {
                        ["<Esc>"] = "Close",
                    },
                },
                win_options = {
                    winhighlight = "NormalFloat:FloatBorder",
                },
                override = function(conf)
                    conf.title = string.gsub(conf.title, "^ ", "")
                    return conf
                end,
            },
            select = {
                backend = "telescope",
            },
        },
    },
    {
        "nvim-tree/nvim-web-devicons",
        opts = {
            override = {
                wgsl = {
                    icon = Icons.wgsl,
                    color = "#8ED8F5",
                    cterm_color = "51",
                    name = "WGSL",
                },
                qml = {
                    icon = Icons.qml,
                    color = "#41CD52",
                    cterm_color = "48",
                    name = "QML",
                },
            },
        },
    },
    {
        "lewis6991/gitsigns.nvim",
        opts = {
            signs = {
                add = { text = Icons.git.add },
                change = { text = Icons.git.change },
                changedelete = { text = Icons.git.changedelete },
                delete = { text = Icons.git.delete },
                topdelete = { text = Icons.git.topdelete },
            },
            signs_staged = {
                changedelete = { text = Icons.git.changedelete },
            },
            numhl = true,
            attach_to_untracked = true,
        },
    },
    {
        "utilyre/barbecue.nvim",
        dependencies = "SmiteshP/nvim-navic",
        opts = { theme = "catppuccin" },
    },
    { "folke/todo-comments.nvim", config = true },
    { "petertriho/nvim-scrollbar", config = true },
    { "NvChad/nvim-colorizer.lua", config = true },
    { "lukas-reineke/indent-blankline.nvim", main = "ibl", opts = { exclude = { filetypes = { "dashboard" } } } },
}
