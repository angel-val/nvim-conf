local dynsplit = {}

----- Dynamic splitting utility -----

-- Setup plugin

local config

function dynsplit.setup(opts)
    config = vim.tbl_deep_extend("keep", opts, {
        charsize = {
            height = 3,
            width = 1,
        },
        dyn_help = true,
        dyn_help_manpage = true,
        dyn_help_swallow_empty = true,
    })

    if config.dyn_help then
        local group = Util.augroup("DynsplitHelp")
        Util.autocmd(group, "BufWinEnter", "Dynamically position help windows", function()
            if vim.bo.buftype == "help" then
                local buf = vim.api.nvim_get_current_buf()
                local win = vim.api.nvim_get_current_win()

                vim.api.nvim_win_close(win, false)

                if vim.api.nvim_buf_get_name(0) ~= "" and config.dyn_help_swallow_empty then
                    dynsplit.split(false, buf)
                else
                    vim.api.nvim_win_set_buf(0, buf)
                end
            end
        end)
    end
end

-- Functionality

function dynsplit.split(focus, buffer)
    local _focus = focus or true
    local _buffer = buffer or vim.api.nvim_create_buf(false, false)

    local height = vim.api.nvim_win_get_height(0) * config.charsize.height
    local width = vim.api.nvim_win_get_width(0) * config.charsize.width

    local dir

    if width > height then
        dir = vim.o.splitright and "right" or "left"
    else
        dir = vim.o.splitbelow and "below" or "above"
    end

    return vim.api.nvim_open_win(_buffer, _focus, { split = dir })
end

return dynsplit
