Util.maps({
    { "<C-h>", "<cmd>wincmd h<CR>" },
    { "<C-j>", "<cmd>wincmd j<CR>" },
    { "<C-k>", "<cmd>wincmd k<CR>" },
    { "<C-l>", "<cmd>wincmd l<CR>" },

    { "<M-j>", "<cmd>resize -1<CR>" },
    { "<M-k>", "<cmd>resize +1<CR>" },
    { "<M-h>", "<cmd>vertical resize -1<CR>" },
    { "<M-l>", "<cmd>vertical resize +1<CR>" },

    { "<C-v>", "<cmd>vnew<CR>" },
    { "<C-x>", "<cmd>new<CR>" },
    { "<C-t>", "<cmd>tabnew<CR>" },

    { "<C-return>", require("dynsplit").split },
    { "<C-S-return>", require("terman").open },

    { "<C-q>", "<cmd>q<CR>" },
    { "<C-s>", "<cmd>w<CR>" },

    { "<Esc>", "<C-\\><C-n>", "t" },
    { "<C-tab>", "<cmd>tabnext<CR>", "t" },
    { "<C-S-tab>", "<cmd>tabprev<CR>", "t" },
    { "<C-PageUp>", "<cmd>tabnext<CR>", "t" },
    { "<C-PageDown>", "<cmd>tabprev<CR>", "t" },
}, { "n", "t" }, {})

Util.maps({
    -- Wrapped lines suck in vim? not anymore!
    { "j", "gj" },
    { "k", "gk" },

    -- Sane redo map
    { "<C-r>", "" },
    { "<S-u>", "<cmd>redo<CR>" },

    -- Better tab-indentation
    { "<S-Tab>", "<gv", "v" },
    { "<Tab>", ">gv", "v" },
    { "<S-tab>", "<C-d>", "i" },

    { "<Esc>", "" },
    { "q:", "" },
    { "q", "" },
    { "<C-z>", "" },
}, "n", {})
