return {
    "echasnovski/mini.nvim",
    lazy = false,
    priority = 2000,
    version = "*",
    config = function()
        require("mini.cursorword").setup()
        require("mini.surround").setup({
            mappings = {
                delete = "ds",
                replace = "cs",
            },
            n_lines = 100,
        })
        --         require("mini.starter").setup({
        --             autoopen = true,
        --             evaluate_single = false,
        --             -- Items to be displayed. Should be an array with the following elements:
        --             -- - Item: table with <action>, <name>, and <section> keys.
        --             -- - Function: should return one of these three categories.
        --             -- - Array: elements of these three types (i.e. item, array, function).
        --             -- If `nil` (default), default items will be used (see |mini.starter|).
        --             items = nil,
        --
        --             header = [[
        --     _   __                _            \n
        --    / | / /__  ____ _   __(_)___ ___     \n
        --   /  |/ / _ \\/ __ \\ | / / / __ `__ \\ \n
        --  / /|  /  __/ /_/ / |/ / / / / / / /    \n
        -- /_/ |_/\\___/\\____/|___/_/_/ /_/ /_/
        --                 ]],
        --
        --             footer = nil,
        --
        --             query_updaters = "abcdefghilmnopqrstuvwxyz0123456789_-.",
        --         })
    end,
}
