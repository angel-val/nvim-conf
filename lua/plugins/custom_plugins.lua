return {
    {
        dir = "~/.config/nvim/custom_plugins/terman",
        dev = true,
        config = true,
        lazy = false,
    },
    {
        dir = "~/.config/nvim/custom_plugins/dynsplit",
        dev = true,
        config = true,
        lazy = false,
    },
}
