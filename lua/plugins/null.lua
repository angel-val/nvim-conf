return {
    "nvimtools/none-ls.nvim",
    config = function()
        -- local a = require("null-ls").builtins.code_actions
        local d = require("null-ls").builtins.diagnostics
        local f = require("null-ls").builtins.formatting
        local h = require("null-ls").builtins.hover

        local sources = {
            d.clj_kondo,
            d.cmake_lint,
            d.golangci_lint,
            d.trail_space,
            d.zsh,

            f.asmfmt,
            f.clang_format,
            f.cmake_format,
            f.djlint,
            f.gofumpt,
            f.joker,
            f.ocamlformat,
            f.prettierd,
            f.shfmt,
            f.stylelint,
            f.stylua,

            h.dictionary,
        }

        require("null-ls").setup({
            sources = sources,
        })
    end,
}
