return {
    "nvim-lualine/lualine.nvim",
    opts = {
        options = {
            theme = function()
                local theme = require("catppuccin.utils.lualine")("mocha")
                local colors = require("catppuccin.palettes").get_palette("mocha")
                theme.inactive = { a = { fg = colors.text, bg = colors.surface0 } }
                return theme
            end,
            component_separators = { left = "", right = "" },
            section_separators = { left = "", right = "" },
            disabled_filetypes = {},
            globalstatus = true,
        },
        sections = {
            lualine_a = { "nvimtree", "mode" },
            lualine_b = { "branch", "diff", "diagnostics" },
            lualine_c = { "filename" },
            lualine_x = { "filetype" },
            lualine_y = { "progress" },
            lualine_z = { "location" },
        },
        tabline = {
            lualine_a = {
                {
                    "tabs",
                    max_length = vim.o.columns * 0.7,
                    disabled_buftypes = { "nofile" },
                    mode = 1,
                    use_mode_colors = true,
                    symbols = { modified = "" },
                    fmt = function(name, context)
                        local buflist = vim.fn.tabpagebuflist(context.tabnr)

                        local bufcount = #buflist
                        if require("nvim-tree.api").tree.is_visible() then
                            bufcount = bufcount - 1
                        end

                        return name .. " [" .. bufcount .. "]"
                    end,
                },
            },
            lualine_b = {},
            lualine_c = {},
            lualine_x = {},
            lualine_y = {},
            lualine_z = {
                {
                    "windows",
                    max_length = vim.o.columns * 0.3,
                    disabled_buftypes = { "nofile" },
                    use_mode_colors = true,
                    symbols = { modified = "" },
                },
            },
        },
    },
}
