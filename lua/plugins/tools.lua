return {
    {
        "monaqa/dial.nvim",
        keys = {
            { "<M-]>", "<Plug>(dial-increment)" },
            { "<M-[>", "<Plug>(dial-decrement)" },
            { "<M-]>", "<Plug>(dial-increment)", "v" },
            { "<M-[>", "<Plug>(dial-decrement)", "v" },
        },
    },

    {
        "mbbill/undotree",
        keys = {
            { "<leader>u", "<cmd>UndotreeToggle<CR>" },
        },
    },

    {
        "folke/flash.nvim",
        event = "VeryLazy",
        keys = { { ";", "" } },
        opts = {
            modes = {
                char = {
                    jump_labels = true,
                },
            },
        },
    },

    { "windwp/nvim-autopairs", config = true },
    "windwp/nvim-ts-autotag",
    "RRethy/nvim-treesitter-endwise",
}
