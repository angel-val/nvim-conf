return {
    "folke/trouble.nvim",
    keys = {
        { "<leader>w", "<cmd>Trouble diagnostics<CR>" },
    },
    config = {
        focus = true,
        warn_no_results = true,
        open_no_results = true,
        win = {}, -- window options for the results window.
        icons = {
            kinds = Icons.kinds,
        },
    },
}
