return {
    "saghen/blink.cmp",
    lazy = false,
    dependencies = "rafamadriz/friendly-snippets",
    version = "v0.*",
    config = function()
        -- nvim-cmp configuration
        require("blink.cmp").setup({
            kind_icons = Icons.kinds,

            keymap = {
                preset = "enter",
            },

            windows = {
                autocomplete = {
                    selection = "auto_insert",
                },
            },

            highlight = { use_nvim_cmp_as_default = true },
            nerd_font_variant = "normal",

            accept = { auto_brackets = { enabled = true } },
            trigger = { signature_help = { enabled = true } },
        })
    end,
}
