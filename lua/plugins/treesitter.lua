return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
        require("nvim-treesitter.configs").setup({
            ensure_installed = "all",
            ignore_install = { "norg" },
            sync_install = true,

            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,

                -- Parsers to disable
                -- disable = { "c", "rust" },
            },

            indent = { enable = false },
            endwise = { enable = true },
            autotag = { enable = true },
        })
    end,
}
