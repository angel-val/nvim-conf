return {
    "neovim/nvim-lspconfig",
    config = function()
        --- nvim-lspconfig settings ---------------------------------------------------

        local lspconfig = require("lspconfig")

        -- See :h lspconfig-all for server names and requirements
        local servers = {
            "bashls",
            "clangd",
            "clojure_lsp",
            "cmake",
            "cssls",
            "gopls",
            "hls",
            "html",
            "jdtls",
            "jsonls",
            "lua_ls",
            "marksman",
            "nixd",
            "nushell",
            "ocamllsp",
            "pylsp",
            "taplo",
            "rust_analyzer",
            "wgsl_analyzer",
        }

        -- Configure a server upon attaching to a buffer
        -- See :h lspconfig-all for info on individual servers
        for _, lsp in ipairs(servers) do
            lspconfig[lsp].setup({
                capabilities = require("blink.cmp").get_lsp_capabilities(vim.lsp.protocol.make_client_capabilities()),
                settings = {
                    nixd = {
                        nixpkgs = {
                            expr = "import <nixpkgs-unstable> { }",
                        },
                        formatting = {
                            command = { "alejandra" },
                        },
                        options = {
                            nixos = {
                                expr = '(builtins.getFlake ("git+file://" + toString ./.)).nixosConfigurations."veronica".options',
                            },
                            home_manager = {
                                expr = '(builtins.getFlake ("git+file://" + toString ./.)).homeConfigurations."angel@veronica".options',
                            },
                        },
                    },
                    Lua = {
                        runtime = { version = "LuaJIT" },
                        workspace = {
                            checkThirdParty = false,
                            library = {
                                vim.env.VIMRUNTIME,
                                "${3rd}/luv/library",
                                "${3rd}/busted/library",
                            },
                        },
                        format = { enable = false },
                    },
                    ["rust-analyzer"] = {
                        checkOnSave = {
                            command = "clippy",
                        },
                    },
                    haskell = {
                        formattingProvider = "fourmolu",
                    },
                },
            })
        end
    end,
}
