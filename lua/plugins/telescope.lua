local tb = require("telescope.builtin")
return {
    "nvim-telescope/telescope.nvim",
    lazy = false,
    dependencies = "burntsushi/ripgrep",
    config = function()
        require("telescope").setup({
            defaults = {
                mappings = {
                    i = {
                        ["<C-h>"] = "which_key",
                        ["<C-d>"] = "delete_buffer",
                        ["<esc>"] = "close",
                    },
                },
                file_ignore_patterns = {
                    ".meta",
                    "include",

                    -- Binary/image/etc files
                    ".jpg",
                    ".png",
                    ".kra",
                    ".gif",
                    ".woff",
                    ".woff2",
                    ".ttf",
                    ".otf",
                },
                borderchars = { " ", " ", " ", " ", " ", " ", " ", " " },
            },
            pickers = {},
            extensions = {},
        })

        local group = Util.augroup("TelescopeOptions")

        -- Set options for telescope preview buffers
        Util.autocmd(group, "User", "Enable Line Number in Telescope Preview", function()
            vim.wo.number = true
        end, { pattern = "TelescopePreviewerLoaded" })
    end,
    keys = {
        { "<C-o>", tb.find_files },
        { "<leader>g", tb.live_grep },
        { "<leader>h", tb.help_tags },
        { "<leader>k", tb.keymaps },
        { "<leader>b", tb.buffers },
    },
}
