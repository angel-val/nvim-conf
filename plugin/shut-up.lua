local group = Util.augroup("ShutUp")

-- Eliminates various minor annoyances

Util.autocmd(group, "BufEnter", "Disable newline comments and other such things", function()
    vim.opt.formatoptions:remove({ "c", "r", "o" })
end)

Util.autocmd(group, "BufWritePre", "Create missing folders when writing a file", function()
    -- Get directory using vim's excellent expand utility (hover method for details)
    local dir = vim.fn.expand("<afile>:p:h")

    -- Ignores files opened using URLs
    if dir:find("%l+://") == 1 then
        return
    end

    -- Create directory if it doesn't exist
    if vim.fn.isdirectory(dir) == 0 then
        vim.fn.mkdir(dir, "p")
    end
end)

-- BufWinLeave triggers when a buffer is detached from its last window
Util.autocmd(group, "BufEnter", "Close empty hidden buffers automatically", function()
    local bufinfos = vim.fn.getbufinfo({ buflisted = 1 })
    for _, bufinfo in ipairs(bufinfos) do
        local buf_empty = vim.api.nvim_buf_get_name(bufinfo.bufnr) == ""
        local buf_no_windows = (not bufinfo.windows or #bufinfo.windows == 0)

        if buf_empty and buf_no_windows then
            -- So that it happens after the window closes
            vim.schedule(function()
                -- Ignore any errors lol
                pcall(vim.api.nvim_buf_delete, bufinfo.bufnr, { force = true, unload = true })
            end)
        end
    end
end)
