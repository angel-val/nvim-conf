Util = {}

---------- Autocmd utilities ----------

function Util.augroup(name)
    return vim.api.nvim_create_augroup(name, { clear = true })
end

--- @param group integer
--- @param event string|string[]
--- @param desc string
--- @param callback function
--- @param opts? vim.api.keyset.create_autocmd
function Util.autocmd(group, event, desc, callback, opts)
    local _opts = opts or {}

    _opts.group = group
    _opts.desc = desc
    _opts.callback = callback

    return vim.api.nvim_create_autocmd(event, _opts)
end

---------- Key mapping utilities ----------

--- @class util.checked_keymap
--- @field [1] string LHS
--- @field [2] string|function RHS
--- @field [3]? string|string[] Mode (default n)
--- @field [4]? vim.keymap.set.Opts Options (default {})

--- @param map util.checked_keymap
function Util.map(map)
    local opts = map[4] or {}

    opts.noremap = true

    vim.keymap.set(map[3] or "n", map[1], map[2], opts)
end

--- @param maps util.checked_keymap[]
--- @param default_mode string|string[]
--- @param default_opts vim.keymap.set.Opts
function Util.maps(maps, default_mode, default_opts)
    for _, map in ipairs(maps) do
        if map[3] == nil then
            map[3] = default_mode
        end
        if map[4] == nil then
            map[4] = default_opts
        end
        Util.map(map)
    end
end
