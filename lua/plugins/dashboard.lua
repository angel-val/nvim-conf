return {
    "goolord/alpha-nvim",
    dependencies = "nvim-tree/nvim-web-devicons",
    lazy = false,
    priority = 70,
    config = function()
        local alpha = require("alpha")
        local dashboard = require("alpha.themes.startify")

        -- Set header
        dashboard.section.header.val = {
            "    _   __                _         ",
            "   / | / /__  ____ _   __(_)___ ___ ",
            "  /  |/ / _ \\/ __ \\ | / / / __ `__ \\",
            " / /|  /  __/ /_/ / |/ / / / / / / /",
            "/_/ |_/\\___/\\____/|___/_/_/ /_/ /_/ ",
        }

        dashboard.opts.autocmd = true
        dashboard.file_icons.provider = "devicons"

        alpha.setup(dashboard.opts)

        local group = Util.augroup("AlphaWins")
        Util.autocmd(group, "User", "When alpha loads", function()
            -- require("trouble").open({ mode = "diagnostics", focus = false, win = { size = { height = 5 } } })
            require("nvim-tree.api").tree.toggle({ focus = false })
        end, { pattern = "AlphaReady" })
    end,
}
