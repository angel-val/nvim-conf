local terman = {}

----- Terminal manager -----

require("terman.autocmds")

-- Setup plugin

local config

function terman.setup(opts)
    config = vim.tbl_deep_extend("keep", opts, {
        split_if_occupied = true,
        focus_already_open = true,
    })
end

-- Functionality

local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local teleconfig = require("telescope.config").values
local actions = require("telescope.actions")
local action_state = require("telescope.actions.state")

local function split_if_occupied()
    if vim.api.nvim_buf_get_name(0) ~= "" and config.split_if_occupied then
        require("dynsplit").split(true)
    end
end

function terman.open(teleopts)
    teleopts = teleopts or {}

    local termbufs = vim.tbl_filter(function(bufnr)
        if 1 ~= vim.fn.buflisted(bufnr) then
            return false
        end
        if teleopts.show_all_buffers == false and not vim.api.nvim_buf_is_loaded(bufnr) then
            return false
        end
        if teleopts.ignore_current_buffer and bufnr == vim.api.nvim_get_current_buf() then
            return false
        end

        if vim.bo[bufnr].buftype ~= "terminal" then
            return false
        end

        return true
    end, vim.api.nvim_list_bufs())

    if #termbufs == 0 then
        split_if_occupied()
        vim.cmd.terminal()
    else
        table.insert(termbufs, -1)

        local finder = finders.new_table({
            results = termbufs,
            entry_maker = function(entry)
                if entry == -1 then
                    return {
                        value = "New",
                        ordinal = "New",
                        display = "New",
                        bufnr = -1,
                        path = "New",
                        filename = "New",
                        lnum = 0,
                        indicator = "",
                    }
                else
                    local bufname = vim.api.nvim_buf_get_name(entry)
                    return {
                        value = bufname,
                        ordinal = bufname,
                        display = bufname,
                        bufnr = entry,
                        path = bufname,
                        filename = bufname,
                        lnum = 0,
                        indicator = "",
                    }
                end
            end,
        })

        local action = function(prompt_bufnr)
            actions.close(prompt_bufnr)

            local selection = action_state.get_selected_entry()
            local bufnr = selection.bufnr

            if selection.display == "New" then
                split_if_occupied()
                vim.cmd.terminal()
            else
                local bufinfos = vim.fn.getbufinfo({ buflisted = 1 })
                for _, bufinfo in ipairs(bufinfos) do
                    if bufinfo.bufnr == bufnr then
                        local buf_no_windows = (not bufinfo.windows or #bufinfo.windows == 0)
                        if buf_no_windows or not config.focus_already_open then
                            split_if_occupied()
                            vim.api.nvim_win_set_buf(0, bufnr)
                        else
                            vim.api.nvim_set_current_win(bufinfo.windows[1])
                        end
                    end
                end
            end
        end

        local picker = pickers.new(teleopts, {
            prompt_title = "Terminal buffers",
            finder = finder,
            sorter = teleconfig.generic_sorter(teleopts),
            previewer = teleconfig.grep_previewer(teleopts),
            attach_mappings = function(prompt_bufnr)
                actions.select_default:replace(function()
                    action(prompt_bufnr)
                end)
                return true
            end,
        })

        picker:find()
    end
end

return terman
